#!/bin/bash

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
mkdir ~/.ssh
touch ~/.ssh/id_rsa
echo -e "$PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.

chmod a+x ./deploy/disableHostKeyChecking.sh
/bin/bash ./deploy/disableHostKeyChecking.sh

# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVERS=$SERVER

# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
#ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
ALL_SERVERS=${DEPLOY_SERVERS}
echo "ALL_SERVERS ${ALL_SERVERS}"

# Lets iterate over this array and ssh into each EC2 instance
# Once inside.
# 1. Stop the server
# 2. Take a pull
# 3. Start the server

#chmod a+x ./deploy/updateAndRestart.sh

#for server in "${ALL_SERVERS[@]}"
#do
#  echo "deploying to ${server}"
#  ssh jenkins@${server} 'bash -s' < ./deploy/updateAndRestart.sh
#done

## 
echo "Connecting to server test..." 
ssh -i ~/.ssh/id_rsa jenkins@$SERVER 'hostname'

echo "Connecting to server..." 
ssh -i ~/.ssh/id_rsa jenkins@$SERVER 'bash -s' < ./deploy/updateAndRestart.sh
