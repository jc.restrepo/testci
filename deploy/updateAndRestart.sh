#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
cd /opt/jenkins/
rm -rf /opt/jenkins/testci
# clone the repo again
git clone https://gitlab.com/jc.restrepo/testci.git
cd /opt/jenkins/testci

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
#source /home/ubuntu/.nvm/nvm.sh

# stop the previous pm2
#pm2 kill
#npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
#npm install pm2 -g
#npm install
# starting pm2 daemon
#pm2 status
#pm2 status

#cd /opt/jenkins/testci/ci_cd_demo/

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
nohup npm start &

#Verify
sleep 10
curl http://localhost:4000
sleep 30
exit

